const util = require('util')
const fs = require('fs')
const readFile = util.promisify(fs.readFile)

const R = require('ramda')

const DATA_FILE = 'frequency_drift.txt'

const bufferToString = (buffer) => buffer.toString()
const notEmpty = R.compose(R.not, R.isEmpty)
const parseInput = R.pipe(
  bufferToString,
  R.split('\n'),
  R.filter(notEmpty)
)
const frequency = R.pipe(parseInput, R.sum)

readFile(DATA_FILE)
  .then(frequency)
  .then(console.log)
